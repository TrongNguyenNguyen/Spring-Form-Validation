# SpringFormValidation
Spring hỗ trợ validate các field/thuộc tính khi submit form bằng cách sử dụng các annotation validate của hibernate-validator, javax-validation.

Các annotation khai báo trước các field sẽ định nghĩa ràng buộc cho các field đó.

Mặc định các message lỗi sẽ được lấy từ file .properties, nếu không tìm thấy thì nó sẽ thấy theo các message khai báo bên cạnh annotation, nếu không tìm thấy cả 2 chỗ trên thì nó sẽ thấy message mặc định của hibernate-validator và validation-api.

Ví dụ:

- field name không được để trống và chiều dài từ 5-10 ký tự
- field dateOfBirth: không được null, có định dạng là dd/MM/yyyy và phải trước ngày hiện tại

Một số annotation validate khác hay dùng như @Size, @Future, @Pattern… Annotation @Phone là do mình tự định nghĩa

References:

https://spring.io/guides/gs/validating-form-input/

https://docs.spring.io/spring/docs/4.1.x/spring-framework-reference/html/validation.html
